$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/testng/login.feature");
formatter.feature({
  "name": "LoginFeature",
  "description": "  This feature deals with the log functionnary of the application.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@loginfeature"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Login with correct username ans password",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I navigate to the login page",
  "keyword": "Given "
});
formatter.step({
  "name": "I enter \u003cusername\u003e and \u003cpassword\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "I should see the admin page \u003cusername\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "myadmin",
        "Password2016"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Login with correct username ans password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@loginfeature"
    }
  ]
});
formatter.step({
  "name": "I navigate to the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.TestGiven()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter myadmin and Password2016",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.TestWhen(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see the admin page myadmin",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.TestThen(String)"
});
formatter.result({
  "status": "passed"
});
});