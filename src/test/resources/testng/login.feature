#Author: madevincre
@loginfeature
Feature: LoginFeature

  This feature deals with the log functionnary of the application.
  
  Scenario Outline: Login with correct username ans password  
  Given I navigate to the login page
  When I enter <username> and <password>
  Then I should see the admin page <username>
  
Examples: 
	| username  	| password 		|
	| myadmin	 	| Password2016	|
		