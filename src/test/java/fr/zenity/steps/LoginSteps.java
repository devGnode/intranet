package fr.zenity.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

/**/
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import fr.zenity.bo.Order;

public class LoginSteps{

	private WebDriver 		driver;
	private WebDriverWait 	wait;
	
	
	@Given("I navigate to the login page")
	public void TestGiven(  ) throws PendingException{
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Driver/chromedriverWIN32.exe");
		this.driver = new ChromeDriver( new ChromeOptions().addArguments("disable-infobars") );
		this.driver.manage().window().maximize();
		this.driver.get("http://intranet-test.zenity-test.fr/");
		this.wait = new WebDriverWait( this.driver, 10 );
		
		System.out.println( "Given method" );
	}
	
	@When("I enter (\\w+) and (\\w+)")
	public void TestWhen( String name, String pass ) throws PendingException{
		
		List<WebElement> form;
		
		/*
		 * konsole check xpath 
		 * $x( xpath )
		 * */
		form = this.driver.findElements( By.xpath("//form[contains(@id,'loginform')]/p/label/input") );
		form.get(0).sendKeys( name );
		form.get(1).sendKeys( pass );
		
		// button callback
		this.driver.findElement( By.id("wp-submit") ).click( );
		// log
		System.out.println("When method logger :="+ name + " / password:="+ pass );
	}
	
	@Then("I should see the admin page (\\w+)")
	public void TestThen( String user ) throws PendingException{
		
		// check verification
		assertTrue( 
				(new String("Salutations, "+user)).equals(
				this.driver.findElement( By.xpath("//a[contains(@class,'ab-item') and contains(@href,'https://intranet-test.zenity-test.fr/wp-admin/profile.php')]") ).getAttribute("innerText")
		) );
		
	
		// quit
		this.driver.quit();
		System.out.println("into then :="+ user );
		
	}
	
	
}
