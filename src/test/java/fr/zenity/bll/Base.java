package fr.zenity.bll;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;

public class Base {

	protected WebDriver driver;
	
	public Base(  ){
		
		//
		System.setProperty(
			"webdriver.opera.driver",
			"Driver/operadriverWIN32.exe"
		);
		
		this.driver = new OperaDriver();
	}
}
