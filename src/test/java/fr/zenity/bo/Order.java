package fr.zenity.bo;

import java.util.ArrayList;
import java.util.List;

public class Order {

	private String owner;
	private String target;
	private List<String> cocktails;
	
	public Order() {
		
		this.cocktails = new ArrayList<String>();
	
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List getCocktails() {
		return cocktails;
	}

	public void setCocktails(List cocktails) {
		this.cocktails = cocktails;
	}
	
	
}
