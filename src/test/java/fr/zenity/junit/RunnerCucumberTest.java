package fr.zenity.junit;

/*
 * Cucumber
 * */
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/*
 * junit
 * */
import org.junit.runner.RunWith;

@RunWith( Cucumber.class ) // run a test with JUnit
@CucumberOptions( 
				features = {"src/test/resources/junit"},
				strict = true,							// strict 
				monochrome = true,						// enabled or disabled color 
				plugin = { "pretty", "html:outHTMLJUnit", "json:outJSONJUnit/reporting.json"} // reporting
)
public class RunnerCucumberTest {

	public RunnerCucumberTest() {}
	
}
