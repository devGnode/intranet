package fr.zenity.junit;

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import fr.zenity.bo.Order;

public class CocktailSteps {

	private Order odr;
	
	@Given("(\\w+) who wants to buy a drink")
	public void TestGiven( String name ) throws PendingException{
		
		this.odr = new Order();
		this.odr.setOwner( name );
		System.out.println("into given" + name );
	}
	
	@When("an order is declared for (\\w+)")
	public void TestWhen( String name ) throws PendingException{
		
		this.odr.setTarget( name );
		System.out.println("into When"+ name );
	}
	
	@Then("there is (\\d+) cocktail in the order")
	public void TestThen( int nbcock ) throws PendingException{
		
		System.out.println("into then"+ nbcock );
		assertEquals( nbcock, this.odr.getCocktails().size( ) );
		
	}
	
	
}
