$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/junit/cocktail.feature");
formatter.feature({
  "name": "Cocktail Ordering",
  "description": "  As Romeo, I want to offer a drink to Juliette so that we can discuss together (and maybe more).",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Creating an empty order",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\u003cfrom\u003e who wants to buy a drink",
  "keyword": "Given "
});
formatter.step({
  "name": "an order is declared for \u003cto\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "there is \u003ccocktail\u003e cocktail in the order",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "from",
        "to",
        "cocktail"
      ]
    },
    {
      "cells": [
        "Romeo",
        "Juliet",
        "0"
      ]
    },
    {
      "cells": [
        "Pierrot",
        "Laure",
        "0"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating an empty order",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Romeo who wants to buy a drink",
  "keyword": "Given "
});
formatter.match({
  "location": "CocktailSteps.TestGiven(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "an order is declared for Juliet",
  "keyword": "When "
});
formatter.match({
  "location": "CocktailSteps.TestWhen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "there is 0 cocktail in the order",
  "keyword": "Then "
});
formatter.match({
  "location": "CocktailSteps.TestThen(int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Creating an empty order",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Pierrot who wants to buy a drink",
  "keyword": "Given "
});
formatter.match({
  "location": "CocktailSteps.TestGiven(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "an order is declared for Laure",
  "keyword": "When "
});
formatter.match({
  "location": "CocktailSteps.TestWhen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "there is 0 cocktail in the order",
  "keyword": "Then "
});
formatter.match({
  "location": "CocktailSteps.TestThen(int)"
});
formatter.result({
  "status": "passed"
});
});